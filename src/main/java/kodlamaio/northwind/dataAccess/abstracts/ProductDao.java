package kodlamaio.northwind.dataAccess.abstracts;

import kodlamaio.northwind.entities.concretes.Product;
import org.springframework.data.jpa.repository.JpaRepository;

//interface interfacei extend eder
public interface ProductDao extends JpaRepository<Product, Integer> {
//    <Entity adı, Primary key alan tipi>  -->  CRUD operasyonları hazır
}
